import { useEffect, useState } from 'react';
import { getDocs } from '../services';
import { getPage } from '../text';
import { useLangContext } from './../contexts/LangContext';
import { useAppContext, SET_LOADING } from './../contexts/AppContext';
import doc from './../static/doc.png';
import { UPLOADS_URL } from './../env'


export default function Documents() {
    const { appDispatch } = useAppContext();
    const [docs, setDocs] = useState([])
    useEffect(() => {
        appDispatch({
            type: SET_LOADING,
            payload: true
        })
        getDocs()
            .then(res => {
                setDocs(res.data)
            })
            .catch(err => {
                appDispatch({ type: SET_LOADING, payload: false})
            })
            .finally(res => {
                appDispatch({ type: SET_LOADING, payload: false})
            })
    }, [])
    const { lang } = useLangContext();
    return (
        <div className="page">
            <h1 className="page__heading">{getPage(lang, 'documents')}</h1>
            <div className="Doc">
                {docs.map((item , idx) => (
                    <a target="_blank" href={`${UPLOADS_URL}${item.uploadedFile.path}`} key={item['_id']} className="Doc__item">
                        <div className="img" style={{backgroundImage: `url('${doc}')`}}></div>
                        <p className="Doc__title">{item.description}</p>
                    </a>
                ))}
            </div>
        </div>
    )
}
