const API_URL = "https://nazarbayevfund.kz/admin/api"
const UPLOADS_URL = "https://nazarbayevfund.kz/admin/public/uploads/"
// const API_URL = "http://localhost:8000/admin/api"
// const UPLOADS_URL = "http://localhost:8000/admin/public/uploads/"
export {
    API_URL,
    UPLOADS_URL
}